# Custom Gloves

## 1 项目简介

远程自定义手套，利用它，远程控制你的计算机！

## 2 项目地址

GitLab：https://gitlab.com/EmisonLu/custom-gloves

## 3 运行环境

操作系统：Windows或macOS

环境：>Python3.0、pyserial、pyautogui

安装方法：

```
pip install pyserial
pip install pyautogui
```

项目运行：

```
python3 manage.py
```

## 4 项目分工

|  人员  |              工作               | GitLab用户名 |
| :----: | :-----------------------------: | :----------: |
| 陆正昊 | 组长、手套制作、Arduino程序编写 |  Emison Lu   |
|  徐扬  |    手套制作、Arduino程序编写    |   Yang Xu    |
| 陆千禧 |          PCB电路板绘制          | shjdluqianxi |
|  沈琪  |           客户端程序            |    MKMQ99    |
| 唐铭江 |           客户端程序            |  kre777777   |

import serial #pyserial

class Port:
    def __init__(self, com, baud=400, time_out=0.2):
        self.ser = serial.Serial(com, baud, timeout=time_out)

    # def expandData(self, string):
    #     for i in range(7,18):
    #         if string[i] == "0" or string[i] == "1":
    #             string += "00"
    #         else:
    #             string[i] = "0"
    #             string += "11"
    #     return string

    def getData(self):
        str_data = self.ser.readline()

        # str_data = str_data.decode()
        str_data = str(str_data)
        str_data = str_data.rstrip()
        if len(str_data) != 11:
            return ""
        # print(str_data)
        return str_data

    def flush(self):
        self.ser.flushInput()
    
    def writeData(self,info):
        self.ser.write(info.encode())


    def writeData(self,info):
        self.ser.write(info.encode())


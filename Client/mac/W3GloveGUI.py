# coding=UTF-8
import tkinter as tk
from tkinter import ttk
import tkinter.messagebox
import pyautogui
import time
from getPortData import Port
from serial.tools import list_ports
from tkinter import filedialog, dialog, messagebox
import os
import json
import threading
import sys

# p_Arrow = '..\\image\\arrow.png'
# p_Plus = '..\\image\\plus.png'
p_Arrow = './image/arrow.png'
p_Plus = './image/plus.png'

# from pystray import MenuItem as item
# import pystray
# from PIL import Image
# from SysTrayIcon import SysTrayIcon


def GUI_start():
    GUIwindow = tk.Tk()
    GUI = W3GloveGUI(GUIwindow)
    GUI.init_window()
    GUIwindow.mainloop()


class W3GloveGUI():
    def __init__(self, window_obj):
        # 所有的手势列表：
        # 大拇指 食指 中指 无名指 小指
        # 左移 右移 前移 后移 上移 下移
        # Roll轴顺时针 Roll轴逆时针
        # Pitch轴顺时针 Pitch轴逆时针
        # Yaw轴顺时针 Yaw轴逆时针
        print(os.path.dirname(os.path.dirname(__file__)))
        self.Motion = ('Thumb', 'Index Finger', 'Middle Finger', 'Ring Finger', 'Little Finger',
                       'Move Right', 'Move Left', 'Forward', 'Backward', 'Upward', 'Downward',
                       'Pitch Clockwise', 'Pitch Anticlockwise',
                       'Roll Anticlockwise', 'Roll Clockwise',
                       'Yaw Clockwise', 'Yaw Anticlockwise')

        # 所有的键鼠列表（键盘可添加）
        # 鼠标：左击 右击 双击 滚轮向上 滚轮向下
        # 键盘：各个按键名
        self.Mouse = ('Left Click', 'Right Click',
                      'Double Click', 'Scroll Up', 'Scroll Down')
        self.Keyboard = ('ctrl', 'shift', 'alt', 'esc', 'tab',
                         'enter', 'fn', 'printscreen', 'space', 'win', 'capslock', 'delete', 'backspace',
                         'f1', 'f2', 'f3', 'f4', 'f5', 'f6', 'f7', 'f8', 'f9', 'f10', 'f11', 'f12',
                         '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                         'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                         'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'right', 'left', 'up', 'down'
                         )

        self.window = window_obj
        # 映射表
        self.mapping = {}  # 手势对应字符串与键鼠行为的映射表

        self.screenWidth, self.screenHeight = pyautogui.size()  # 获取屏幕尺寸

        self.mapping_show_x = 40
        self.mapping_show_y = 330

        self.showing_count = 0

        self.showing_motion = []
        self.showing_action = []
        self.showing_arrow = []

        self.flag = True  # for start and stop
        self.photo_arrow = tk.PhotoImage(file=p_Arrow).subsample(10, 10)
        self.photo_plus = tk.PhotoImage(file=p_Plus).subsample(10, 10)

        self.comboxlist_width = 11
        self.comboxlist_height = 20
        self.comboxlist1_x = 70
        self.comboxlist1_y = 130

    def refresh_combobox(self):
        print("hello, scan ports now")
        self.port_list = list(list_ports.comports())
        self.comboxlist_port["values"] = self.port_list

    def init_window(self):
        self.window.title('Custom Glove')
        # self.window.bind("<Unmap>", lambda event: self.Hidden_window(
        # ) if self.window.state() == 'iconic' else False)

        self.window.geometry('760x850')
        self.window.resizable(0, 0)

        # screenwidth = self.window.winfo_width()  # 获取整个窗口的宽度
        # screenheight = self.window.winfo_height()  # 获取整个窗口的高度

        # setting 窗口
        canvas_setting = tk.Canvas(
            self.window, bg='white', height=330, width=900)
        canvas_setting.place(x=30, y=20)

        canvas_setting.create_line(25, 329, 660, 329, fill="light gray", width=3)
        canvas_setting.create_line(25, 800, 660, 800, fill="light gray", width=3)

        l_setting = tk.Label(self.window, text='SETTING',
                             font=("Arial", 22, 'bold'), bg='white')
        l_setting.place(x=320, y=22)
        l_motion = tk.Label(self.window, text='Gestures',
                            font=("Arial", 16), bg='white')
        l_motion.place(x=90, y=90)

        l_action = tk.Label(self.window, text='Keyboard & Mouse',
                            font=("Arial", 16), bg='white')
        l_action.place(x=300, y=90)

        b_set = tk.Button(self.window, text='Set', width=10,
                          height=2, command=self.set_mapping)
        b_set.place(x=275, y=280)
        b_reset = tk.Button(self.window, text='Reset', width=10,
                            height=2, command=self.reset_mapping)
        b_reset.place(x=395, y=280)

        # mapping 窗口
        canvas_mapping = tk.Canvas(
            self.window, bg='white', height=430, width=900)
        canvas_mapping.place(x=30, y=370)

        canvas_mapping.create_line(25, 348, 660, 348, fill="light gray", width=3)

        l_mapping = tk.Label(self.window, text='MAPPING',
                             font=("Arial", 22, 'bold'), bg='white')
        l_mapping.place(x=323, y=380)

        b_clear = tk.Button(self.window, text='Clear', width=10,
                            height=2, command=self.clear_mapping)
        b_clear.place(x=337, y=650)
        b_start = tk.Button(self.window, text='Start',
                            width=11, height=2, command=self.start, bg='#90EE90')
        b_start.place(x=268, y=763)
        b_stop = tk.Button(self.window, text='Stop', width=11,
                           height=2, command=self.stop, bg='#FA8072')
        b_stop.place(x=408, y=763)

        self.operation_area()
        self.menu_bar()
        self.window.config(menu=self.menubar)

        # 选择串口端口
        l_port = tk.Label(self.window, text='Serial Port',
                          font=("Arial", 16), bg='white')
        l_port.place(x=590, y=90)

        self.port_list = list(list_ports.comports())

        comvalue_port = tk.StringVar().set('')
        self.comboxlist_port = ttk.Combobox(self.window, postcommand=self.refresh_combobox, textvariable=comvalue_port, font=(
            'Arial', 15), width=11, height=5)  # 初始化
        self.comboxlist_port["values"] = self.port_list
        self.comboxlist_port.place(x=570, y=130)

    def menu_bar(self):
        self.menubar = tk.Menu(self.window)
        filemenu = tk.Menu(self.menubar, tearoff=0)
        self.menubar.add_cascade(label='File', menu=filemenu)
        filemenu.add_command(label='Open', command=self.open_job)
        filemenu.add_command(label='Save', command=self.save_job)
        filemenu.add_separator()
        filemenu.add_command(label='Exit', command=self.window.quit)

        helpmenu = tk.Menu(self.menubar, tearoff=0)
        self.menubar.add_cascade(label='Help', menu=helpmenu)
        helpmenu.add_command(label='About', command=self.about_job)

    def about_job(self):
        root = tk.Tk()
        root.title('Magic Glove 1.0')
        root.geometry('500x200')
        text = '''Author : Zhenghao Lu, Yang Xu, Mingjiang Tang, Qi Shen, Qianxi Lu

Date : 2021/06/01'''
        tk.Label(root, text=text, font=('Arial', 12, 'bold')).place(x=40, y=50)
        root.mainloop()

    def operation_area(self):
        # 加号按钮的图案
        # 8个下拉框

        # Gesture
        comvalue_motion1 = tk.StringVar().set('')
        self.comboxlist_motion1 = ttk.Combobox(
            self.window, textvariable=comvalue_motion1, font=('Arial', 15), width=self.comboxlist_width, height=self.comboxlist_height)  # 初始化
        self.comboxlist_motion1["values"] = self.Motion
        self.comboxlist_motion1.place(
            x=self.comboxlist1_x, y=self.comboxlist1_y)
        self.add_motion1 = tk.Button(
            self.window, image=self.photo_plus, command=self.add_motion_1)
        self.add_motion1.place(
            x=self.comboxlist1_x+self.comboxlist_width + 130, y=self.comboxlist1_y)  # button 255

        comvalue_motion2 = tk.StringVar().set('')
        self.comboxlist_motion2 = ttk.Combobox(
            self.window, textvariable=comvalue_motion2, font=('Arial', 15), width=self.comboxlist_width, height=self.comboxlist_height)  # 初始化
        self.comboxlist_motion2["values"] = self.Motion
        self.add_motion2 = tk.Button(
            self.window, image=self.photo_plus, command=self.add_motion_2)

        comvalue_motion3 = tk.StringVar().set('')
        self.comboxlist_motion3 = ttk.Combobox(
            self.window, textvariable=comvalue_motion3, font=('Arial', 15), width=self.comboxlist_width, height=self.comboxlist_height)  # 初始化
        self.comboxlist_motion3["values"] = self.Motion

        # keyborad & ..
        comvalue_action1 = tk.StringVar().set('')
        self.comboxlist_action1 = ttk.Combobox(
            self.window, textvariable=comvalue_action1, font=('Arial', 15), width=self.comboxlist_width, height=self.comboxlist_height)  # 初始化
        self.comboxlist_action1["values"] = self.Mouse + self.Keyboard
        self.comboxlist_action1.place(x=310, y=self.comboxlist1_y)
        self.add_action1 = tk.Button(
            self.window, image=self.photo_plus, command=self.add_action_1)
        self.add_action1.place(x=450, y=self.comboxlist1_y)

        comvalue_action2 = tk.StringVar().set('')
        self.comboxlist_action2 = ttk.Combobox(
            self.window, textvariable=comvalue_action2, font=('Arial', 15), width=self.comboxlist_width, height=self.comboxlist_height)  # 初始化
        self.comboxlist_action2["values"] = self.Mouse + self.Keyboard
        self.add_action2 = tk.Button(
            self.window, image=self.photo_plus, command=self.add_action_2)

        comvalue_action3 = tk.StringVar().set('')
        self.comboxlist_action3 = ttk.Combobox(
            self.window, textvariable=comvalue_action3, font=('Arial', 15), width=self.comboxlist_width, height=self.comboxlist_height)  # 初始化
        self.comboxlist_action3["values"] = self.Mouse + self.Keyboard

    def add_motion_1(self):
        self.comboxlist_motion2.place(
            x=self.comboxlist1_x, y=self.comboxlist1_y+50)
        self.add_motion2.place(x=211, y=180)
        self.add_motion1.pack_forget()

    def add_motion_2(self):
        self.comboxlist_motion3.place(x=self.comboxlist1_x, y=self.comboxlist1_y+100)
        self.add_motion2.pack_forget()

    def add_action_1(self):
        self.comboxlist_action2.place(x=310, y=180)
        self.add_action2.place(x=450, y=180)
        self.add_action1.pack_forget()

    def add_action_2(self):
        self.comboxlist_action3.place(x=310, y=230)
        self.add_action2.pack_forget()

    def KeyboardMouse(self, args):
        print("args", args)
        if len(args) == 1:
            if args[0] in self.Mouse:
                if args[0] == 'Left Click':
                    pyautogui.click()
                    print("click")
                elif args[0] == 'Right Click':
                    pyautogui.rightClick()
                elif args[0] == 'Double Click':
                    pyautogui.doubleClick()
                elif args[0] == 'Scroll Up':
                    pyautogui.scroll(clicks=200)
                elif args[0] == 'Scroll Down':
                    pyautogui.scroll(clicks=-200)

            elif args[0] in self.Keyboard:
                pyautogui.press(args[0])
        elif len(args) == 2:
            pyautogui.hotkey(args[0], args[1])
        elif len(args) == 3:
            pyautogui.hotkey(args[0], args[1], args[2])

    # 建立映射的函数

    def set_mapping(self):
        # 添加映射，初始化
        mapping_key_bin = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        mapping_value = []

        # 手势设定为空，跳出提示，并退出函数
        if (self.comboxlist_motion1.get() == '' and
                self.comboxlist_motion2.get() == '' and
                self.comboxlist_motion3.get() == ''
                ):
            return

        if self.comboxlist_motion1.get() != '':
            mapping_key_bin[self.Motion.index(
                self.comboxlist_motion1.get())] = 1
        if self.comboxlist_motion2.get() != '':
            mapping_key_bin[self.Motion.index(
                self.comboxlist_motion2.get())] = 1
        if self.comboxlist_motion3.get() != '':
            mapping_key_bin[self.Motion.index(
                self.comboxlist_motion3.get())] = 1

        # 键鼠行为设定为空，跳出提示，并退出函数
        if (self.comboxlist_action1.get() == '' and
                self.comboxlist_action2.get() == '' and
                self.comboxlist_action3.get() == ''):
            return

        if self.comboxlist_action1.get() != '':
            mapping_value.append(self.comboxlist_action1.get())
        if self.comboxlist_action2.get() != '':
            mapping_value.append(self.comboxlist_action2.get())
        if self.comboxlist_action3.get() != '':
            mapping_value.append(self.comboxlist_action3.get())

        mapping_key = ''.join(map(str, mapping_key_bin))
        # 如果该手势已经被设定，跳出提示，并退出函数
        if mapping_key in self.mapping:
            return
        else:
            self.mapping[mapping_key] = mapping_value
        # print(mapping_key)

        # 显示映射关系
        showing_str1 = ''
        showing_str2 = ''

        for i in range(len(mapping_key)):
            if mapping_key[i] == '1':
                showing_str1 += self.Motion[i] + ' + '
        showing_str1 = showing_str1[:-3]

        for x in mapping_value:
            showing_str2 += x + ' + '
        showing_str2 = showing_str2[:-3]

        # print(showing_str1)
        # print(showing_str2)
        y_public = 90
        x_public = 50
        self.showing_motion.append(tk.Label(self.window, text=showing_str1, height=2, width=34, font=('Arial', 15),
                                            wraplength=320, justify='left'))
        self.showing_motion[self.showing_count].place(
            x=self.mapping_show_x + x_public, y=self.mapping_show_y + self.showing_count * 50 + y_public)
        self.showing_action.append(tk.Label(self.window, text=showing_str2, height=2, width=20, font=('Arial', 15),
                                            wraplength=180, justify='left'))
        self.showing_action[self.showing_count].place(
            x=self.mapping_show_x + x_public + 327, y=self.mapping_show_y + self.showing_count * 50 + y_public)

        self.showing_arrow.append(
            tk.Label(self.window, image=self.photo_arrow, bg='white'))
        self.showing_arrow[self.showing_count].place(
            x=self.mapping_show_x + x_public + 281, y=self.mapping_show_y + self.showing_count * 50 + y_public+8)

        self.showing_count += 1

        # 清空选择框，回到初始状态
        self.reset_mapping()

    # 重新设置的函数

    def reset_mapping(self):
        # 清空选择框，回到初始状态
        self.comboxlist_motion1.set('')
        self.comboxlist_motion2.set('')
        self.comboxlist_motion3.set('')
        self.comboxlist_action1.set('')
        self.comboxlist_action2.set('')
        self.comboxlist_action3.set('')

        self.comboxlist_motion2.pack_forget()
        self.comboxlist_motion3.pack_forget()
        self.comboxlist_action2.pack_forget()
        self.comboxlist_action3.pack_forget()

        self.add_motion2.pack_forget()
        self.add_action2.pack_forget()

        self.add_motion1.place(x=211, y=130)
        self.add_action1.place(x=450, y=130)

    # 清空所有映射的函数

    def clear_mapping(self):
        # 清空映射
        self.mapping.clear()

        # 清空显示
        for x in self.showing_motion:
            x.destroy()
        for x in self.showing_action:
            x.destroy()
        for x in self.showing_arrow:
            x.destroy()
        self.showing_motion.clear()
        self.showing_action.clear()
        self.showing_arrow.clear()
        self.showing_count = 0

    # -----------------------------------

    def portstr2mystr(self, portstr):
        mystr = portstr[0:5]
        for x in portstr[5:]:
            if x == '0':
                mystr += '00'
            elif x == '1':
                mystr += '10'
            elif x == '2':
                mystr += '01'
        return mystr

    def start(self):
        PortData = Port(self.comboxlist_port.get()[0:22])
        # PortData = Port("/dev/cu.usbmodem142301")
        time.sleep(1)
        pyautogui.moveTo(self.screenWidth / 2,
                         self.screenHeight / 2, duration=0.25)
        # 这里应用串口通信传入信号
        self.flag = True
        print(self.mapping)

        while self.flag:
            # PortData.flush()
            self.window.update()
            if not self.flag:
                return
            x = PortData.getData()
            if len(x) != 0:
                print(x)
                # PortData.flush()
                # print(portstr2mystr(x))
            # time.sleep(1)z
            if self.portstr2mystr(x) in self.mapping.keys():
                self.KeyboardMouse(self.mapping[self.portstr2mystr(x)])
                time.sleep(1)
                PortData.flush()

    def stop(self):
        self.flag = False
        print("stop")

    def open_job(self):
        file_path = filedialog.askopenfilename(
            title=u'选择文件', initialdir=(os.path.expanduser('F:/')))
        print('打开文件：', file_path)
        if file_path is not None:
            with open(file_path, 'r') as f:
                self.mapping = json.load(f)
            print(self.mapping)

        key = list(self.mapping.keys())
        value = list(self.mapping.values())
        print(key)
        print(value)

        self.showing_count = len(key)

        for i in range(self.showing_count):
            showing_str1 = ''
            showing_str2 = ''
            mapping_key = key[i]
            mapping_value = value[i]
            for j in range(len(mapping_key)):
                if mapping_key[j] == '1':
                    showing_str1 += self.Motion[j] + ' + '
            showing_str1 = showing_str1[:-3]

            for x in mapping_value:
                showing_str2 += x + ' + '
            showing_str2 = showing_str2[:-3]

            self.showing_motion.append(tk.Label(self.window, text=showing_str1, height=2, width=34, font=('Arial', 15),
                                                wraplength=320, justify='left'))
            self.showing_motion[i].place(
                x=self.mapping_show_x, y=self.mapping_show_y + i * 50)
            self.showing_action.append(tk.Label(self.window, text=showing_str2, height=2, width=20, font=('Arial', 15),
                                                wraplength=180, justify='left'))
            self.showing_action[i].place(
                x=self.mapping_show_x + 360, y=self.mapping_show_y + i * 50)

            self.showing_arrow.append(
                tk.Label(self.window, image=self.photo_arrow, bg='white'))
            self.showing_arrow[i].place(
                x=self.mapping_show_x + 325, y=self.mapping_show_y + i * 50 + 10)

            y_public = 90
            x_public = 50
            self.showing_motion.append(tk.Label(self.window, text=showing_str1, height=2, width=34, font=('Arial', 15),
                                                wraplength=320, justify='left'))
            self.showing_motion[i].place(
                x=self.mapping_show_x + x_public, y=self.mapping_show_y + i * 50 + y_public)
            self.showing_action.append(tk.Label(self.window, text=showing_str2, height=2, width=20, font=('Arial', 15),
                                                wraplength=180, justify='left'))
            self.showing_action[i].place(
                x=self.mapping_show_x + x_public + 327, y=self.mapping_show_y + i * 50 + y_public)

            self.showing_arrow.append(
                tk.Label(self.window, image=self.photo_arrow, bg='white'))
            self.showing_arrow[i].place(
                x=self.mapping_show_x + x_public + 281, y=self.mapping_show_y + i * 50 + y_public+8)

    def save_job(self):
        file_opt = options = {}
        options['filetypes'] = [('text files', '.json'), ('all files', '.*')]
        file_path = filedialog.asksaveasfile(
            title=u'保存文件', defaultextension=".json", **file_opt)
        print('保存文件：', file_path)
        if file_path is not None:
            json.dump(self.mapping, file_path)
            print('保存完成')
